# AUTOVALA #

## WHAT IS IT? ##

Autovala is a program and a library designed to help in the creation of
projects with Vala and CMake.

The idea is quite simple: CMake is very powerful, but writting the CMakeLists
files is boring and repetitive. Why not let the computer create them, by
guessing what to do with each file? And if, at the end, there are mistakes,
let the user fix them in an easy way, and generate the final CMakeLists files.

This is what Autovala does. This process is done in three steps:

  * First, Autovala checks all the folders and files, and writes a project
    file with the type of each file
  * It also peeks the source files to determine which Vala packages they need,
    and generate automagically that list
  * After that (and after allowing the user to check, if (s)he wishes, the
    project file), it uses that project file to generate the needed CMakeLists
    files

## COMPILING AUTOVALA ##

Just use these commands:

    mkdir install
    cd install
    cmake ..
    make
    sudo make install
    sudo ldconfig

You also can compile the plugins for Gedit and Scratch Text Editor, which are
in the folders *gedit_plugin* and *scratch_plugin*. You can find inside the
instructions.


## USING AUTOVALA ##

The DOC folder contains the Wiki dumped in HTML format. Just open the
**index.html** file with your browser, or go to the **Wiki section** in GitHub,
and enjoy.


## Fork difference ##

The major difference from original version - this fork works out of the box without need for explicit installation.
